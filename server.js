const express = require('express');
const  path = require('path');
const  bodyParser = require('body-parser');
const  cors = require('cors');
const  mongoose = require('mongoose');
const  config = require('./DB');
const  products = require ('./routes/products');
const  category = require ('./routes/category');
const  orders = require ('./routes/orders');
const  flags = require ('./routes/flags');
const  test = require ('./routes/test');
const  tasks = require('./routes/tasks')
var messages = require('./routes/messages');
var mysql      = require('mysql');

mongoose.Promise = global.Promise;
mongoose.connect(config.DB);
      
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cors());

app.use(function(req, res, next) {
  res.locals.connection = mysql.createConnection({
    host     : 'localhost',
	port	 : 3306,
    user     : 'root',
    database : 'foodex'
  });
  res.locals.connection.connect();
  next();
});
app.use('/messages', messages);






app.use('/products', products);
app.use('/test', test);
app.use('/orders', orders);
app.use('/category', category);
app.use('/flags', flags);
app.use('/api', tasks);
app.use(express.static('public'));
var port = process.env.PORT || 4000;

app.listen(port, function(){
  console.log('NodeJS Server Port: ', port);
});