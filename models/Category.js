const mongoose = require('mongoose');
const shortid = require('shortid');

const CategorySchema = mongoose.Schema({
	_id: { type: String, default: shortid.generate
	},
    name: String,
    updated_date: { type: Date, default: Date.now },
});
module.exports = mongoose.model('Category', CategorySchema);