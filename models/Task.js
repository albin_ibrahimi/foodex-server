const Sequelize = require("sequelize");
const db = require("../database/db.js");

module.exports = db.sequelize.define(
  "messages",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    emri: {
      type: Sequelize.STRING
    },
	email: {
      type: Sequelize.STRING
    },
	mesazhi: {
      type: Sequelize.STRING
    },
	date: {
      type: Sequelize.STRING
    }
	
	
  },
  {
    timestamps: false
  }
);
