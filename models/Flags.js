const mongoose = require('mongoose');

const FlagsSchema = mongoose.Schema({
    name: String,
    image: String,
    updated_date: { type: Date, default: Date.now },
});
module.exports = mongoose.model('Flags', FlagsSchema);