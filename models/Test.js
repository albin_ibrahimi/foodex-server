const mongoose = require('mongoose');

const TestSchema = mongoose.Schema({
    name: String,
    foto: String,
    category: String,
    updated_date: { type: Date, default: Date.now },
});
module.exports = mongoose.model('Test', TestSchema);