const mongoose = require('mongoose');
const shortid = require('shortid');


const OrdersSchema = mongoose.Schema({
	_id: {
  type: String,
  default: shortid.generate
	},
	userid: String,
    prodid: String,
	name : String,
    adresa:String,
	nrtelefonit:String,
	sasia:Number,
	status:{ type: Number, default: 0 },
	delivered:{ type: Number, default: 0 },
	confirmedby:String,
    updated_date: { type: Date, default: Date.now },
});
module.exports = mongoose.model('Orders', OrdersSchema);