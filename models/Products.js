const mongoose = require('mongoose');
const shortid = require('shortid');


const ProductsSchema = mongoose.Schema({
	_id: {
  type: String,
  default: shortid.generate
	},
    name: String,
    category:String,
    image:String,
    description:String,
    flag:String,
    stock:Number,
    price:Number,
    deletedby:String,
    isDeleted:{ type:Boolean, default:false},
    updated_date: { type: Date, default: Date.now },
});
module.exports = mongoose.model('Products', ProductsSchema);