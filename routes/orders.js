var Orders = require('../models/Orders');
var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();


/* GET ALL Contacts */
router.get('/', function(req, res, next) {
    Orders.find(function (err, orders) {
      if (err) return next(err);
      res.json(orders);
    });
  });
  
/* GET SINGLE Contacts BY ID */
router.get('/:id', function(req, res, next) {
    Orders.findById(req.params.id, function (err, post) {
      if (err) return next(err);
      res.json(post);
    });
  });

  /* SAVE Contacts */
router.post('/', function(req, res, next) {
    Orders.create(req.body, function (err, post) {
      if (err) return next(err);
      res.json(post);
    });
  });

  /* UPDATE Contacts */
router.put('/:id', function(req, res, next) {
    console.log(req.body);
    Orders.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
      if (err) return next(err);
      res.json(post);
    });
  });
  
  /* DELETE Contacts */
router.delete('/:id', function(req, res, next) {
    Orders.findByIdAndRemove(req.params.id, req.body, function (err, post) {
      if (err) return next(err);
      res.json(post);
    });
  });
  module.exports = router;
