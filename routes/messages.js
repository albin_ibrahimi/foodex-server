var express = require('express');
var router = express.Router();
var _ = require('underscore');
var messages = require('../models/message');
var q = require('q');


router.post('/new', function(req, res, next) {
	q.all([messages.formulateInsertQuery(req.body, 'messages')]).then(function(queryRes){
		if(queryRes[0]){
			q.all([messages.insertFunction(req, res.locals.connection, q, queryRes[0])]).then(function(insertRes){
				if(insertRes[0][0].insertId > 0){
					res.json("success")
				} else {
					res.json("error")
				}
			}).fail(function(err){
				console.log(err.stack)
			})			
		}
	}).fail(function(err){
		console.log(err.stack)
	})
});

module.exports = router;
